require("dotenv").config();

const _ = require("lodash");
const { program } = require("commander");

const { GraphQLClient, gql } = require("graphql-request");

const GITLAB_PRIVATE_TOKEN = process.env.GITLAB_PRIVATE_TOKEN || "SETME";
const GROUP_NAME = process.env.GROUP_NAME || "SETME";
const REQUEST_HEADERS = {};

const USERNAMES_OF_TEAM = JSON.parse(process.env.USERNAMES_OF_TEAM);
const AGGREGATE_BY_USER = process.env.AGGREGATE_BY_USER === "true";

// query from https://gitlab.com/gitlab-org/gitlab-foss/-/issues/25532#note_333937886
const timeLogQuery = gql`
  query getTimeLog(
    $groupName: ID!
    $startDate: Time!
    $endDate: Time!
    $first: Int
    $after: String
  ) {
    group(fullPath: $groupName) {
      fullName
      timelogs(
        startDate: $startDate
        endDate: $endDate
        first: $first
        after: $after
      ) {
        edges {
          node {
            spentAt
            timeSpent
            user {
              username
            }
            issue {
              assignees {
                edges {
                  node {
                    name
                  }
                }
              }
              id
              title
              timeEstimate
              milestone {
                title
              }
              labels {
                edges {
                  node {
                    title
                  }
                }
              }
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`;

const getGraphqlPaged = async (
  query,
  params,
  payloadPath,
  pageInfoPath,
  results = [],
  first = 100,
  after,
) => {
  const client = new GraphQLClient(
    `https://gitlab.com/api/graphql?private_token=${GITLAB_PRIVATE_TOKEN}`,
  );
  const queryPayload = await client.request(
    query,
    { ...params, first, after },
    REQUEST_HEADERS,
  );
  const resultsFromQuery = _.get(queryPayload, payloadPath);
  const pageInfo = _.get(queryPayload, pageInfoPath);
  if (pageInfo?.hasNextPage) {
    return await getGraphqlPaged(
      query,
      params,
      payloadPath,
      pageInfoPath,
      results.concat(resultsFromQuery),
      first,
      pageInfo.endCursor,
    );
  }
  return results.concat(resultsFromQuery);
};

const objectArrayAsCSV = (data) => {
  const headers = Object.keys(data[0]).join();
  const content = data.map((r) => Object.values(r).join());
  return [headers].concat(content).join("\n");
};

const getReport = async (startDate, endDate) => {
  const timeLogs = await getGraphqlPaged(
    timeLogQuery,
    {
      groupName: GROUP_NAME,
      startDate,
      endDate,
    },
    "group.timelogs.edges",
    "group.timelogs.pageInfo",
  );
  const timeLogsOfTeam =
    Array.isArray(USERNAMES_OF_TEAM) && USERNAMES_OF_TEAM.length > 0
      ? timeLogs.filter((timelog) =>
          USERNAMES_OF_TEAM.includes(_.get(timelog, "node.user.username")),
        )
      : timeLogs;
  // console.debug(JSON.stringify(timeLogs));

  const report = timeLogsOfTeam.reduce((report, current) => {
    const currentUsername = AGGREGATE_BY_USER
      ? _.get(current, "node.user.username")
      : "no_user_aggregate";
    const currentIssueId = _.get(current, "node.issue.id");
    if (!(currentUsername in report)) report[currentUsername] = {};
    if (!(currentIssueId in report[currentUsername]))
      report[currentUsername][currentIssueId] = _.get(current, "node.issue");
    // Filter negative spent which can happen when we reset the spent for a new sprint
    report[currentUsername][currentIssueId]["timeSpent"] =
      (_.get(current, "node.timeSpent") > 0
        ? _.get(current, "node.timeSpent")
        : 0) + (report[currentUsername][currentIssueId]["timeSpent"] || 0);
    return report;
  }, {});
  const reportEntries = Object.entries(report)
    .flatMap(([username, issues]) => {
      return Object.entries(issues).flatMap(([issueId, issueEntry]) => {
        const estimateFormatted = issueEntry.timeEstimate / 36 / 100;
        const spentFormatted = issueEntry.timeSpent / 36 / 100;
        const estimateDelta = Number.isInteger(issueEntry.timeEstimate)
          ? issueEntry.timeEstimate - issueEntry.timeSpent
          : 0;
        const estimateDeltaFormatted = estimateDelta / 36 / 100;
        const accuracy =
          estimateFormatted !== 0
            ? (100 * estimateDeltaFormatted) / estimateFormatted
            : 0;
        return {
          ...(AGGREGATE_BY_USER
            ? {
                username,
              }
            : null),
          issueId,
          title: issueEntry.title,
          assignees: issueEntry.assignees.edges
            .map((edge) => edge.node.name)
            .join(";"),
          labels: issueEntry.labels.edges
            .map((edge) => edge.node.title)
            .join(";"),
          milestone: issueEntry.milestone.title,
          estimateFormatted,
          spentFormatted,
          estimateDeltaFormatted,
          accuracy: Math.round(accuracy * 100) / 100,
        };
      });
    })
    .filter((entry) => entry.spentFormatted != 0);
  return reportEntries;
};

const timelog = async (startDate, endDate) => {
  const reportEntries = await getReport(startDate, endDate);
  const reportEntriesSorted = _.sortBy(reportEntries, "accuracy");
  // console.debug(JSON.stringify(reportEntriesSorted));
  const csv = objectArrayAsCSV(reportEntriesSorted);
  console.log(csv);
};

const timelogForLabel = async (startDate, endDate, labelName) => {
  const reportEntries = await getReport(startDate, endDate);
  const reportEntriesForLabel = reportEntries.filter((entry) =>
    entry.labels
      .split(";")
      .find((entryLabelName) => entryLabelName == labelName),
  );
  const sorted = _.sortBy(reportEntriesForLabel, "milestone");
  sorted.forEach((entry) => {
    console.log(`${entry.milestone}: ${entry.spentFormatted}`);
  });
};

(async () => {
  program
    .command("timelog", { isDefault: true })
    .argument("<startDate>")
    .argument("[endDate]")
    .description("Get timelog for time period")
    .action(async (startDate, endDate = new Date()
      .toISOString()
      .substring(0, 10)) => {
      await timelog(startDate, endDate);
    });
  program
    .command("timelog-sysops")
    .argument("<startDate>")
    .argument("[endDate]")
    .description("Get timelog for all issues with label sys-ops")
    .action(async (startDate, endDate = new Date()
      .toISOString()
      .substring(0, 10)) => {
      await timelogForLabel(startDate, endDate, "type::duty::sys-operations");
    });
  program
    .command("timelog-optimizationops")
    .argument("<startDate>")
    .argument("[endDate]")
    .description("Get timelog for all issues with label sys-ops")
    .action(async (startDate, endDate = new Date()
      .toISOString()
      .substring(0, 10)) => {
      await timelogForLabel(startDate, endDate, "type::duty::optimization-operations");
    });
  program.parse();
})();
