# gitlab-timelog

Get time logs for a group within a given start- and end date from gitlab.com.

## Run

The GitLab private token requires read-only permissions on the API.

```bash
npm install
GITLAB_PRIVATE_TOKEN=XY GROUP_NAME=nexoya START_DATE=2022-04-01 END_DATE=2022-04-17 node index.js
```
